This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "data-miner-manager-cl"



## [v1.11.0]

 - Updated maven-parent 1.2.0
 - Updated gcube-bom 2.4.1



## [v1.10.0] - 2023-01-12

 - Added support to default value for ListParameter [#24026]



## [v1.9.0] - 2021-10-06

 - Added cluster description in Service Info [#19213]



## [v1.8.0] - 2019-10-01

- Added service info [#12594]
- Added support to show log information [#11711]
- Added support to show files html, json, pdf, txt [#17106]
- Updated information show to the user when a computation is submitted [#17030]
- Added Item Id support [#16503]
- Updated ComputationId to support token in process url [#17030]



## [v1.7.0] - 2019-04-01

- Added location and zoom support [#11708]
- Added coordinates EPSG:4326 and EPSG:3857 support [#11710]



## [v1.6.0] - 2019-01-31

- Updated to support get operator by id with refresh



## [v1.5.0] - 2018-10-01

- Updated to support StorageHub[#11720]



## [v1.4.0] - 2018-06-01

- Updated to support netcdf files
- Added support for operators refresh[#11741]



## [v1.3.0] - 2017-06-12

- Support Java 8 compatibility [#8471]



## [v1.2.0] - 2017-05-01

- Updated to support DataMinerManagerWidget [#6078]
- Fixed load balancing [#7576]



## [v1.1.0] - 2017-03-20

- Updated Monitor interface
- Added encoded parameters in equivalent http request [#7167]



## [v1.0.0] - 2017-02-14

- first release



