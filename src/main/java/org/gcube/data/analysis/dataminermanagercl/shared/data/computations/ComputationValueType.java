package org.gcube.data.analysis.dataminermanagercl.shared.data.computations;

/**
 * 
 * @author Giancarlo Panichi
 * 
 *
 */
public enum ComputationValueType {
	FileList, File, Image, String;
}
